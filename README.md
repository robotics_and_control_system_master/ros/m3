[[_TOC_]]


## Helpful commands 

### Convert a .xacro file to .urdf file

```bash
rosrun xacro xacro XACRO_PATH > URDF_EXPORT_PATH
```

### Check URDF model

```bash
check_urdf URDF_PATH
```

### Graphic diagram

```bash
urdf_to_graphiz URDF_PATH
```

### Visualizing URDF models with rviz

```bash
roslaunch urdf_tutorial display.launch gui:=true model:=ABSOLUTE_PATH_TO_URDF_MODEL
```

## Relevant links

### URDF and Xacro

- **URDF (tutorial)**: http://wiki.ros.org/urdf/Tutorials
- **Xacro**: http://wiki.ros.org/xacro

### rviz

- **rviz (user guide)**: http://wiki.ros.org/rviz/UserGuide
